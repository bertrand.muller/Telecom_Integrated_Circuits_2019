library ieee;

use ieee.std_logic_1164.all;

entity DecoderSelect is
	port(
		Entree : in  std_logic_vector(1 downto 0);
		Sortie : out std_logic_vector(3 downto 0)
	);
end DecoderSelect;

architecture ConcSelect of DecoderSelect is
	begin
		with Entree select
			Sortie <= "0001" when "00",
						 "0010" when "01",
						 "0100" when "10",
						 "1000" when "11",
						 "0000" when others;
end ConcSelect;