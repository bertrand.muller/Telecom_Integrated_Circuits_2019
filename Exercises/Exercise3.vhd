library ieee;

use ieee.std_logic_1164.all;

entity Exercise3 is
	port(
		Entree : in  std_logic_vector(2 downto 0);
		Sortie : out std_logic_vector(1 downto 0)
	);
end Exercise3;

architecture ProConc of Exercise3 is
	begin
		Sortie(0) <= Entree(2) or (not Entree(2) and not Entree(1) and Entree(0));
		Sortie(1) <= Entree(2);
end ProConc;