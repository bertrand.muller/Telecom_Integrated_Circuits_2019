library ieee;

use ieee.std_logic_1164.all;

entity Exercise1 is
	port(
		IN0,IN1,IN2,IN3,IN4: in  std_logic_vector(7 downto 0);
		Selecteur			 : in  std_logic_vector(2 downto 0);
		Sortie 				 : out std_logic_vector(7 downto 0)
	);
end Exercise1;

architecture ArchCase of Exercise1 is begin
	process(Selecteur,IN0,IN1,IN2,IN3,IN4)
	--process(all) –VHDL 2008
	begin
		case Selecteur is
			when "000" 	=> Sortie <= IN0;
			when "001" 	=> Sortie <= IN1;
			when "010" 	=> Sortie <= IN2;
			when "011" 	=> Sortie <= IN3;
			when "100" 	=> Sortie <= IN4;
			when others => Sortie <= (others => '0');
		end case;
	end process;
end ArchCase;