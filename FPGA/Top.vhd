library ieee;
 
use ieee.std_logic_1164.all;
 
entity Top is
	port(
		SW		: in std_logic_vector(9 downto 0);
		HEX0	: out std_logic_vector(6 downto 0);
		HEX1	: out std_logic_vector(6 downto 0);
		LEDR	: out std_logic_vector(9 downto 0)
	);
end Top;


architecture archTop of Top is

	component ALU is
		port(
			a, b	: in std_logic_vector(3 downto 0);
			c		: in std_logic_vector(1 downto 0);
			s		: out std_logic_vector(3 downto 0)
		);
	end component;
	
	signal ledrOut : std_logic_vector(9 downto 0);
	signal hx0, hx1 : std_logic_vector(6 downto 0);
	
begin

	unit: ALU
		port map(a => SW(3 downto 0), b => SW(7 downto 4), c => SW(9 downto 8), s => ledrOut(3 downto 0));
		
	LEDR <= ledrOut;
	
	process(ledrOut)
		begin
			case ledrOut(3 downto 0) is
				when "0000" => hx0 <= "0111111"; -- 0
				when "0001" => hx0 <= "0000110"; -- 1
				when "0010" => hx0 <= "1011011"; -- 2
				when "0011" => hx0 <= "1001111"; -- 3
				when "0100" => hx0 <= "1100110"; -- 4
				when "0101" => hx0 <= "1101101"; -- 5
				when "0110" => hx0 <= "1111101"; -- 6
				when "0111" => hx0 <= "0000111"; -- 7
				when "1000" => hx0 <= "1111111"; -- 8
				when "1001" => hx0 <= "1101111"; -- 9
				when "1010" => hx0 <= "0111111"; -- 10
				when "1011" => hx0 <= "0000110"; -- 11
				when "1100" => hx0 <= "1011011"; -- 12
				when "1101" => hx0 <= "1001111"; -- 13
				when "1110" => hx0 <= "1100110"; -- 14
				when "1111" => hx0 <= "1101101"; -- 15
			end case;
		
			case ledrOut(3 downto 0) is
				when "1010" => hx1 <= "0000110"; -- 10
				when "1011" => hx1 <= "0000110"; -- 11
				when "1100" => hx1 <= "0000110"; -- 12
				when "1101" => hx1 <= "0000110"; -- 13
				when "1110" => hx1 <= "0000110"; -- 14
				when "1111" => hx1 <= "0000110"; -- 15
				when others => hx1 <= "0000000"; -- 0
			end case;
			
			HEX0 <= not hx0;
			HEX1 <= not hx1;
	end process;
end archTop;