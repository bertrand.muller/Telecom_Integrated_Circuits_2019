library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity reg2x4 is
	generic 
	(
		DATA_WIDTH : natural := 4
	);

	port 
	(
		clk 		: in  std_logic;
	in1,in2 	: in  std_logic_vector(DATA_WIDTH-1 downto 0);
	ld_all, ld_in2 	: in  std_logic;
	q1,q2	: out std_logic_vector((DATA_WIDTH -1) downto 0)
); 
end entity;

architecture rtl of reg2x4 is
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if ld_all = '1' then
				q1 <= in1;
				q2 <= in2;
			elsif ld_in2 = '1' then
				q2 <= in2;
			end if; 
		end if;
	end process;
end;





