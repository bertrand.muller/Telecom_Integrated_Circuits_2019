library ieee;
use ieee.std_logic_1164.all;


entity ir_dec is 
port(
	clk : in std_logic;
	reset : in std_logic;
     ir:  in  std_logic_vector(7 downto 0);
     carry:  in  std_logic;
     zero:  in  std_logic;
     jump_pc : out std_logic;
     inc_pc : out std_logic;
     ld_ir : out std_logic;
     ld_ir_lsn : out std_logic;
     write : out std_logic;
     en_accu : out std_logic;
     aop : out std_logic_vector(2 downto 0));
end;

architecture beh of ir_dec is 
	type fsm is (fetch,decode,execute);
	signal curr_s, next_s 	: fsm;
	constant load_rd			: std_logic_vector(3 downto 0):="0100";
   constant add_rd			: std_logic_vector(3 downto 0):="0101";
   constant xor_rd			: std_logic_vector(3 downto 0):="0110";
   constant and_rd			: std_logic_vector(3 downto 0):="0111";
begin

	process(clk)
	begin
		if (rising_edge(clk)) then
			if reset = '1' then
				curr_s <= fetch;
			else
			       	curr_s <= next_s;
			end if;
		end if;
	end process;

	process(all)
	begin
		jump_pc   <= '0';
		inc_pc    <= '0';
		ld_ir     <= '0';
		ld_ir_lsn <= '0';
		write     <= '0';
		en_accu   <= '0';
		aop       <= "000";
		case curr_s is
			when fetch   =>
				next_s <= decode;
				ld_ir  <= '1';
				inc_pc <= '1';
			when decode  =>
				next_s <= execute;
				if (ir(7 downto 4) = load_rd) or   
				   (ir(7 downto 4) = add_rd) or   
				   (ir(7 downto 4) = xor_rd) or   
				   (ir(7 downto 4) = and_rd) then   
					   ld_ir_lsn <= '1';
				   end if;
			when execute =>
				next_s <= fetch;
				case? ir is
					when "0100----" =>
						aop <= "001";
						en_accu<= '1';
					when "0001----" =>
						aop <= "001";
						en_accu<= '1';
					when "0011----" =>
						aop <= "001";
						write <= '1';
					when "0101----" =>
						aop <= "010";
						en_accu<= '1';
					when "0010----" =>
						aop <= "010";
						en_accu<= '1';
					when "0110----" =>
						aop <= "011";
						en_accu<= '1';
					when "0111----" =>
						aop <= "100";
						en_accu<= '1';
					when "00000000" =>
						aop <= "110";
					when "00000001" =>
						aop <= "101";
					when "00000010" =>
						if carry = '1' then
						       	inc_pc <= '1';
						end if;
					when "00000011" => 
						if zero = '1' then
						       	inc_pc <= '1';
						end if;
					when "1-------" =>
				       	jump_pc   <= '1'; 
				when others =>
			       	end case?;
		end case;
	end process;
end;
