-- Quartus II VHDL Template
-- Single-Port ROM

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rom_proc is

	generic 
	(
		DATA_WIDTH : natural := 8;
		ADDR_WIDTH : natural := 7
	);

	port 
	(
		addr	: in std_logic_vector(ADDR_WIDTH-1 downto 0);
		--addr	: in natural range 0 to 2**ADDR_WIDTH - 1;
		q		: out std_logic_vector((DATA_WIDTH -1) downto 0)
	);

end entity;

architecture rtl of rom_proc is
	subtype word_t is std_logic_vector((DATA_WIDTH-1) downto 0);
	type memory_t is array(2**ADDR_WIDTH-1 downto 0) of word_t;

	function init_rom
		return memory_t is 
		variable tmp : memory_t := (others => (others => '0'));
	begin 
		-- Initialisation de la ROM avec le programme
		-- Quartus générera un fichier mif associé
		tmp(0):= x"00";	-- clear\_c
		tmp(1):= x"40";	-- charge R0 
		tmp(2):= x"52";	-- add R2
		tmp(3):= x"34";	-- store R4 
		tmp(4):= x"41";	-- load R1
		tmp(5):= x"53";	-- add R3
		tmp(6):= x"35";	-- store R5
		tmp(7):= x"87";	-- halt: jump halt
		tmp(16#10#):= x"AA";	-- Valeur de la donnée à l'adresse 0x10
		tmp(16#11#):= x"55";	-- Valeur de la donnée à l'adresse 0x11
		return tmp;
	end init_rom;	 

	signal rom : memory_t := init_rom;

begin

	process(addr)
	begin
		q <= rom(to_integer(unsigned(addr)));
	end process;

end rtl;

