library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity counter is 
	generic (SIZE : integer := 7);
	port(
		    CLK		: in	std_logic;
		    RESET	: in	std_logic;
		    LOAD	: in	std_logic;
		    ENABLE	: in	std_logic;
		    DIN	        : in	std_logic_vector(SIZE-1 downto 0);
		    DOUT	: out	std_logic_vector(SIZE-1 downto 0));
end;
architecture behavior of counter is begin
	clk_proc:process(CLK)
		variable COUNT:unsigned(SIZE-1 downto 0) := (others => '0');
	begin 
		if rising_edge(CLK) then
			if RESET = '1' then 
				COUNT := (others=>'0');
			else
				if ENABLE = '1' then 
					COUNT := COUNT + 1;
					if COUNT = 2**SIZE-1 then
						COUNT := (others => '0');
					end if; 
				elsif LOAD = '1' then
					COUNT := unsigned(DIN);
				end if;
			end if;
		end if;
		DOUT <= std_logic_vector(COUNT);
	end process clk_proc;
end behavior;
