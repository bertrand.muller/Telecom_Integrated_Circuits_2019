library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity processor is
	port (clk 	: 	in std_logic;
	      reset 	: 	in std_logic);
end;

architecture rtl of processor is
	component counter  
	generic (SIZE : integer := 7);
	port(
		    CLK		: in	std_logic;
		    RESET	: in	std_logic;
		    LOAD	: in	std_logic;
		    ENABLE	: in	std_logic;
		    DIN	        : in	std_logic_vector(SIZE-1 downto 0);
		    DOUT	: out	std_logic_vector(SIZE-1 downto 0));
	end component;
	component ram_proc 
	generic 
	(
		DATA_WIDTH : natural := 4;
		ADDR_WIDTH : natural := 4
	); 
	port 
	(
		addr	: in std_logic_vector(ADDR_WIDTH-1 downto 0);
		data	: in std_logic_vector((DATA_WIDTH-1) downto 0);
		write		: in std_logic := '1';
		q		: out std_logic_vector((DATA_WIDTH -1) downto 0)
	); 
	end component;
	component rom_proc 
	generic 
	(
		DATA_WIDTH : natural := 8;
		ADDR_WIDTH : natural := 7
	); 
	port 
	(
		addr	: in std_logic_vector(ADDR_WIDTH-1 downto 0);
		q		: out std_logic_vector((DATA_WIDTH -1) downto 0)
	); 
	end component;
	component reg2x4 
	generic 
	(
		DATA_WIDTH : natural := 4
	); 
	port 
	(
	clk 		: in  std_logic;
	in1,in2 	: in  std_logic_vector(DATA_WIDTH-1 downto 0);
	ld_all, ld_in2 	: in  std_logic;
	q1,q2		: out std_logic_vector((DATA_WIDTH -1) downto 0)); 
	end component;
	component mux_proc 
	generic 
	(
		DATA_WIDTH : natural := 4
	); 
	port 
	(
		in1,in2 : in  std_logic_vector(DATA_WIDTH-1 downto 0);
		sel 	: in  std_logic;
		q	: out std_logic_vector((DATA_WIDTH -1) downto 0)
	); 
	end component;
	component alu_proc 
	generic (DATA_WIDTH: integer:=4);
	port(
	  clk          : in std_logic;
	  reset          : in std_logic;
	  en_accu      : in std_logic;
	op1,op2      	: in  std_logic_vector(DATA_WIDTH-1 downto 0);
	aop 		: in  std_logic_vector(2 downto 0);
	accu      	: out  std_logic_vector(DATA_WIDTH-1 downto 0);
	carry      	: out  std_logic;
	zero      	: out  std_logic);
	end component;
	component ir_dec port(
	     clk : in std_logic;
	     reset : in std_logic;
	     ir:  in  std_logic_vector(7 downto 0);
	     carry:  in  std_logic;
	     zero:  in  std_logic;
	     jump_pc : out std_logic;
	     inc_pc : out std_logic;
	     ld_ir : out std_logic;
	     ld_ir_lsn : out std_logic;
	     write : out std_logic;
	     en_accu : out std_logic;
	     aop : out std_logic_vector(2 downto 0));
	end component;


	signal jump_pc,inc_pc,write,en_accu 	  	: std_logic;
	signal carry,zero,ld_ir,ld_ir_lsn	 	: std_logic;
	signal ad_rom 	 				: std_logic_vector(6 downto 0);
	signal ir,rom 					: std_logic_vector(7 downto 0);
	signal ram,mux_out,accu				: std_logic_vector(3 downto 0);
	signal aop					: std_logic_vector(2 downto 0);
begin
	pc_count_1: counter generic map (7) port map (clk,reset,jump_pc,inc_pc,ir(6 downto 0),ad_rom);
	rom_1: rom_proc generic map (8,7) port map (ad_rom,rom);
	reg_1: reg2x4 generic map (4) port map (clk,rom(7 downto 4),mux_out, ld_ir, ld_ir_lsn,ir(7 downto 4), ir(3 downto 0));
	mux_1: mux_proc generic map(4) port map (rom(3 downto 0),ram,ld_ir_lsn,mux_out);
	ram_1: ram_proc generic map(4,4) port map(ir(3 downto 0),accu,write,ram);
	alu_1: alu_proc generic map(4) port map (clk,reset,en_accu,ir(3 downto 0),accu,aop,accu,carry,zero);
	ird_1: ir_dec port map (clk,reset,ir,carry,zero,jump_pc,inc_pc,ld_ir,ld_ir_lsn,write,en_accu,aop);
end;
