transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -2008 -work work {T:/SCIN/Telecom_Integrated_Circuits_2019/InternalMemory/rom_proc.vhd}
vcom -2008 -work work {T:/SCIN/Telecom_Integrated_Circuits_2019/InternalMemory/reg2x4.vhd}
vcom -2008 -work work {T:/SCIN/Telecom_Integrated_Circuits_2019/InternalMemory/ram_proc.vhd}
vcom -2008 -work work {T:/SCIN/Telecom_Integrated_Circuits_2019/InternalMemory/processor.vhd}
vcom -2008 -work work {T:/SCIN/Telecom_Integrated_Circuits_2019/InternalMemory/mux_proc.vhd}
vcom -2008 -work work {T:/SCIN/Telecom_Integrated_Circuits_2019/InternalMemory/ir_dec.vhd}
vcom -2008 -work work {T:/SCIN/Telecom_Integrated_Circuits_2019/InternalMemory/counter.vhd}
vcom -2008 -work work {T:/SCIN/Telecom_Integrated_Circuits_2019/InternalMemory/alu_proc.vhd}

vcom -2008 -work work {T:/SCIN/Telecom_Integrated_Circuits_2019/InternalMemory/tb_processor.vhd}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cyclonev -L rtl_work -L work -voptargs="+acc"  tb_processor

add wave *
view structure
view signals
run -all
