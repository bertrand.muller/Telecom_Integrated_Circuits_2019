library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity mux_proc is
	generic 
	(
		DATA_WIDTH : natural := 4
	);

	port 
	(
		in1,in2 : in  std_logic_vector(DATA_WIDTH-1 downto 0);
		sel 	: in  std_logic;
		q	: out std_logic_vector((DATA_WIDTH -1) downto 0)
	); 
end entity;

architecture rtl of mux_proc is
begin
	process(all)
	begin
		if sel = '1' then
			q <= in2;
		else
			q <= in1;
		end if; 
	end process;
end;



