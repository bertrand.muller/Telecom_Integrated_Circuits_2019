library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity alu_proc is 
	generic (DATA_WIDTH: integer:=4);
	port(
	  clk          : in std_logic;
	  reset          : in std_logic;
	  en_accu      : in std_logic;
	op1,op2      	: in  std_logic_vector(DATA_WIDTH-1 downto 0);
	aop 		: in  std_logic_vector(2 downto 0);
	accu      	: out  std_logic_vector(DATA_WIDTH-1 downto 0);
	carry      	: buffer  std_logic;
	zero      	: out  std_logic);
end;

architecture rtl of alu_proc is 
	signal res : std_logic_vector(DATA_WIDTH downto 0);
	signal carry_l,zero_l: std_logic;
	signal carry_en,zero_en: std_logic;
begin
	process(all)
		variable res_tmp : std_logic_vector(DATA_WIDTH downto 0);
		variable carry_compl : std_logic_vector(DATA_WIDTH-1 downto 0);
		variable carry_int : integer range 0 to 1;
	begin
		--if rising_edge(clk) then
			carry_l <= '0';
			carry_en <= '0';
			zero_en <= '0';
			zero_l  <= '0';
			carry_compl:=(others=>'0');
			case aop is
				when "001" => res_tmp := '0' & op1;
				when "010" => 
					res_tmp := std_logic_vector(unsigned('0'&op1)+unsigned('0'&op2)+unsigned(carry_compl& carry));
				       	carry_l <= res_tmp(DATA_WIDTH);
					carry_en <= '1';
			       	when "011" => res_tmp := '0' & (op1 xor op2);
			       	when "100" => res_tmp := '0' & (op1 and op2);
			       	when "101" => carry_l <= '1'; carry_en <= '1';
			       	when "110" => carry_l <= '0'; carry_en <= '1';
			       	when others => res_tmp := (others => '0');
		end case;
	       	if (res_tmp) = std_logic_vector(to_unsigned(0,DATA_WIDTH)) then
			zero_l <= '1'; zero_en <= '1';
		end if; 
		res <= res_tmp;
		--end if;
	end process;

	process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				accu <= (others => '0');
			elsif en_accu = '1' then
				accu <= res(DATA_WIDTH-1 downto 0);
			end if;
		       	if reset = '1' then
				carry <= '0';
			elsif carry_en = '1' then
				carry <= carry_l;
			end if;
		       	if reset = '1' then
				zero <= '0';
			elsif carry_en = '1' then
				zero <= zero_l;
			end if;

		end if;
	end process;
end;
