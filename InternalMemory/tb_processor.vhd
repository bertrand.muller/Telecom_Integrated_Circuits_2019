library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_processor is
end;

architecture beh of tb_processor is
	component processor 
	port (clk 	: 	in std_logic;
	      reset 	: 	in std_logic);
	end component;
	signal clk,reset : std_logic:='0'; 
begin
	proc_1: processor port map(clk,reset);
	clk <= not clk after 5 ns;
	reset <= '1', '0' after 26 ns;
end;
