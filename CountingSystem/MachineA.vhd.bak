library ieee;
use ieee.std_logic_1164.all;

entity MachineA is
port(
	clk, T, I, reset: in std_logic;
	L, D, V, F: out std_logic
);
end MachineA ;

architecture MachineA_arch of MachineA is
	type mc_state_type is (As, Bs, Cs, Ds, Es);
	signal state_reg, state_next: mc_state_type;
	attribute enum_encoding: string;
	attribute enum_encoding of mc_state_type: type is "0000 0100 1000 1001 1010";

	begin
		-- State register
		process(clk, reset)
		begin
			if(reset='1') then
				state_reg <= As;
			elsif(clk'event and clk='1') then
				state_reg <= state_next;
			end if;
		end process;
		
		-- Next-state logic
		process(state_reg, T, I)
		begin
			case state_reg is
				when As =>
					if (I='1') then
						state_next <= Bs;
					end if;
					
				when Bs =>
					state_next <= Cs;
					
				when Cs =>
					if(I='1') then
						if(T='1') then
							state_next <= Es;
						end if;
					else
						state_next <= Ds;
					end if;
					
				when Ds =>
					state_next <= As;
					
				when Es =>
					if(I='0') then
						state_next <= Ds;
					end if;
			end case;
		end process;
		
		-- Moore output logic
		process(state_reg)
		begin
			D <= '0';
			L <= '0';
			V <= '0';
			F <= '0';
			case state_reg is
				when As =>
					
				when Bs =>
					D <= '1';
					L <= '1';
					
				when Cs =>
					D <= '1';
					
				when Ds =>
					F <= '1';
				
				when Es =>
					V <= '1';
					D <= '1';
			end case;
		end process;
end MachineA_arch;