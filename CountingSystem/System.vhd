library ieee;
use ieee.std_logic_1164.all;
use  ieee.numeric_std.all;

entity System is
	port(
		clk, RAZ	: in  std_logic;
		KEY     	: in  std_logic_vector(3 downto 0)
	);
end System ; 

architecture System_arch of System is

	constant n : integer	:= 27;
	constant size : integer := 8;
	 
	component CounterNBits is
		port (
			clk, RAZ, valid 	: in std_logic;
			Q 						: buffer std_logic_vector(n-1 downto 0)
		);
	end component;
	
	component MachineB is 
		port(
			clk, Qnk, RAZ 	: in std_logic;
			S 					: out std_logic
		);
	end component;
	
	component MachineA is
		port(
			clk, T, I, RAZ : in std_logic;
			L, D, V, F		: out std_logic
		);
	end component;
	
	component CounterBCD is
		port(
			clk, RAZ, ena 	: in  std_logic;
			cout 				: out std_logic_vector(n-1 downto 0);
			TC      			: out std_logic
		);
	end component;
	
	component DecoderBCD is
		port(
			Di 	: in  	std_logic_vector(3 downto 0);
			segs 	: out   	std_logic_vector(6 downto 0)
		);
	end component;
	
	component Multiplexer is
		port(
			S, L, V 	: in std_logic;
			Ena		: out std_logic
		);
	end component;
	
	component Synchronizer is
		port(
			clk, Ds	: in std_logic;
			Qs			: out std_logic
		);
	end component;
	
	signal QOut	: std_logic_vector(n-1 downto 0);
	signal segsOut1, segsOut2 : std_logic_vector(6 downto 0);
	signal SOut, EnaOut, LOut, VOut, DOut, FOut, QsOut, TCOut1, TCOut2 : std_logic;
	signal COut1, COut2 : std_logic_vector(3 downto 0);
	
	begin
	
		mapping1: CounterNBits
			port map(clk => clk, RAZ => (not KEY(3) or FOut), valid => DOut, Q => QOut);
			
		mapping2: MachineB
			port map(clk => clk, Qnk => QOut(20), RAZ => KEY(3), S => SOut);
			
		mapping3: Multiplexer
			port map(S => SOut, L => LOut, V => VOut, Ena => EnaOut);
			
		mapping4: Synchronizer
			port map(Ds => not KEY(0), clk => clk, Qs => QsOut);
			
		mapping5: MachineA
			port map(I => QsOut, RAZ => not KEY(3), clk => clk, T => QOut(n-1), F => FOut, L => LOut, V => VOut, D => DOut);
		
		mapping6: CounterBCD
			port map(clk => clk, RAZ => not KEY(3), ena => EnaOut, cout => COut1, TC => TCOut1); -- ena => (EnaOut and COut2)
			
		mapping7: CounterBCD
			port map(clk => clk, RAZ => not KEY(3), ena => EnaOut, cout => COut2, TC => TCOut2);
			
		mapping8: DecoderBCD
			port map(Di => COut1, segs => segsOut1);
			
		mapping9: DecoderBCD
			port map(Di => Cout2, segs => segsOut2);
	
end System_arch;