library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CounterNBits is
	generic(
		n: integer := 27
	);
	
	port(
		clk, RAZ, valid: in std_logic;
		Q: buffer std_logic_vector(n-1 downto 0)
	);
end CounterNBits;

architecture CounterNBits_arch of CounterNBits is
begin
	process(clk, RAZ, valid)
		begin
			if (clk'event and clk = '1') then
				if (RAZ = '1') then
					Q <= (others => '0');
				elsif (valid = '1') then
					Q <= std_logic_vector(unsigned(Q)+1);
				end if;
			end if;
	end process;
end CounterNBits_arch;