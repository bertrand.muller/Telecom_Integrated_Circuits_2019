library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DecoderBCD is
	generic(
		n : integer := 4
	);
	port(
		Di 	: in  	std_logic_vector(n-1 downto 0);
		segs 	: out   	std_logic_vector(6 downto 0)
	);
end DecoderBCD;

architecture DecoderBCD_arch of DecoderBCD is
signal segs_int :  std_logic_vector(6 downto 0);
begin
	process(Di)
	begin
		case Di is
			when "0000" => segs_int <= "1111110";
			when "0001" => segs_int <= "0110000";
			when "0010" => segs_int <= "1101101";
			when "0011" => segs_int <= "1111001";
			when "0100" => segs_int <= "0110011";
			when "0101" => segs_int <= "1011011";
			when "0110" => segs_int <= "1011111";
			when "0111" => segs_int <= "1110000";
			when "1000" => segs_int <= "1111111";
			when "1001" => segs_int <= "1111011";
			when "1010" => segs_int <= "1110111";
			when "1011" => segs_int <= "0011111";
			when "1100" => segs_int <= "1001110";
			when "1101" => segs_int <= "0111101";
			when "1110" => segs_int <= "1001111";
			when "1111" => segs_int <= "1000111";
			when others => segs_int <= "0111110";
		end case;
	end process;
	segs <= not segs_int;
end DecoderBCD_arch;