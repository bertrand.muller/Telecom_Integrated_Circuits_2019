library ieee;
use ieee.std_logic_1164.all;

entity Synchronizer is
	port(
		clk, Ds: in std_logic;
		Qs: out std_logic
	);
end Synchronizer;

architecture Synchronizer_arch of Synchronizer is
begin
		process(clk, Ds)
		begin
			if (clk = '1') then
				Qs <= Ds;
			end if;
		end process;
end Synchronizer_arch;