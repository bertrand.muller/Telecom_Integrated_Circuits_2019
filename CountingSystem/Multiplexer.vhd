library ieee;
use ieee.std_logic_1164.all;

entity Multiplexer is
	port(
		S, L, V 	: in std_logic;
		Ena		: out std_logic
	);
end Multiplexer;

architecture Multiplexer_arch of Multiplexer is
begin
	process(S, L, V)
	begin
		if V = '0' then
			Ena <= L;
		else
			Ena <= S;
		end if;
	end process;
end Multiplexer_arch;