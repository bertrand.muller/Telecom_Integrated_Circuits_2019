library ieee;
use ieee.std_logic_1164.all;

entity MachineB is
	port(
		Qnk, RAZ, clk  : in  std_logic;
		S  : out std_logic
	);
end MachineB;

architecture MachineB_arch of MachineB is
	type state_type is (zero, edge, one);
	signal state_reg, state_next: state_type;
	
	begin
	
		-- Registre d'état
		process(clk, RAZ)
		begin
			if(RAZ='1') then
				state_reg <= zero;
			elsif (clk'event and clk='1') then
				state_reg <= state_next;
			end if;
		end process;
			
		-- Logique de l'état futur
		process(state_reg, Qnk)
		begin
			case state_reg is
				when zero =>
					if Qnk='1' then
						state_next <= edge;
					else
						state_next <= zero;
					end if;
				when edge =>
					if Qnk='0' then
						state_next <= zero;
					else
						state_next <= one;
					end if;
				when one =>
					if Qnk='0' then
						state_next <= zero;
					else
						state_next <= one;
					end if;
			end case;
		end process;
		
		-- Génération de sortie
		process(state_reg)
		begin
			S <= '0';
			case state_reg is
				when edge => S <= '1';
				when zero =>
				when one =>
			end case;
		end process;
end MachineB_arch;