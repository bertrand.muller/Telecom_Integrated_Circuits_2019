library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CounterBCD is
	generic (
		n : integer := 4;
		modulo : integer := 10
	);
	port(
		clk, RAZ, ena 	: in  std_logic;
		cout 				: out std_logic_vector(n-1 downto 0);
		TC      			: out std_logic
	);
end;

architecture CounterBCD_arch of CounterBCD is begin
	clk_proc:process(clk)
		variable count:unsigned(n-1 downto 0) := (others => '0');
	begin
		if rising_edge(clk) then
			if RAZ = '1' then
				count := (others => '0');
			else
				TC <= '1';
				if ena = '0' then
					count := count + 1;
					if count = modulo then
						count := (others => '0');
						TC <= '0';
					end if;
				end if;
			end if;
		end if;
		cout <= std_logic_vector(count);
	end process clk_proc;
end CounterBCD_arch;