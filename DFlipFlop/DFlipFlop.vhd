library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DFlipFlop is
	port(
		clk: in std_logic;
		d: in std_logic;
		r: in std_logic;
		p: in std_logic;
		e: in std_logic;
		q: out std_logic
	);
end DFlipFlop;

architecture arch of DFlipFlop is
begin
	process(clk, r, p)
		begin
			if(r = '1') then
				q <= '0';
			elsif (p = '1') then
				q <= '1';
			elsif (clk'event and clk = '1') then
				if (e = '1') then
				-- if rising_edge(clk) then
					q <= d;
				end if;
			end if;
	end process;
end arch;