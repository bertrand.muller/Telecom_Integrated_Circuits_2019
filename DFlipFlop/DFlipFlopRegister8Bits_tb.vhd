library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DFlipFlopRegister8Bits_tb is
end DFlipFlopRegister8Bits_tb;

architecture testbench2 of DFlipFlopRegister8Bits_tb is
	component DFlipFlopRegister8Bits
		port(
			clk: in std_logic;
			d: in std_logic_vector(7 downto 0);
			r: in std_logic;
			p: in std_logic;
			e: in std_logic;
			q: out std_logic_vector(7 downto 0)
		);
	end component;
	
	signal inClk: std_logic := '0';
	signal inR, inP, inE: std_logic;
	signal inD, outQ: std_logic_vector(7 downto 0);
	
	begin
	
		mapping: DFlipFlopRegister8Bits port map(inClk, inD, inR, inP, inE, outQ);
		inClk <= not inClk after 5ns;
		
		process
			begin
				
				inD <= "00000000";
				inR <= '0';
				inP <= '0';
				inE <= '0';
				wait for 100ns;
				
				
				inD <= "01011101";
				inR <= '0';
				inP <= '0';
				inE <= '0';
				wait for 55ns;
				
				
				inD <= "01011101";
				inR <= '1';
				inP <= '0';
				inE <= '0';
				wait for 50ns;
				
				
				inD <= "00000000";
				inR <= '0';
				inP <= '1';
				inE <= '1';
				wait for 50ns;
				
				
				inD <= "00000000";
				inR <= '0';
				inP <= '0';
				inE <= '1';
				wait for 50ns;
				
				
				inE <= '0';
				wait for 50ns;
				
				
				inD <= "01011101";
				inR <= '1';
				inP <= '1';
				inE <= '0';
				wait for 50ns;
				
		end process;
	
end testbench2;