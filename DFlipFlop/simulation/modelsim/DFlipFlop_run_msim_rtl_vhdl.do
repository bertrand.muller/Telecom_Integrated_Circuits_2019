transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {T:/SCIN/DFlipFlop/DFlipFlop.vhd}
vcom -93 -work work {T:/SCIN/DFlipFlop/DFlipFlopRegister8Bits.vhd}

vcom -93 -work work {T:/SCIN/DFlipFlop/DFlipFlopRegister8Bits_tb.vhd}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cyclonev -L rtl_work -L work -voptargs="+acc"  DFlipFlopRegister8Bits_tb

add wave *
view structure
view signals
run -all
