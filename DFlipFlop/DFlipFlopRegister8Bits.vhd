library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DFlipFlopRegister8Bits is
	port(
		clk: in std_logic;
		d: in std_logic_vector(7 downto 0);
		r: in std_logic;
		p: in std_logic;
		e: in std_logic;
		q: out std_logic_vector(7 downto 0)
	);
end DFlipFlopRegister8Bits;

architecture arch of DFlipFlopRegister8Bits is
	component DFlipFlop is
		port(
			clk: in std_logic;
			d: in std_logic;
			r: in std_logic;
			p: in std_logic;
			e: in std_logic;
			q: out std_logic
		);
	end component;
	
	signal inClk: std_logic := '0';
	signal inR, inP, inE: std_logic;
	signal inD, outQ: std_logic_vector(7 downto 0);
	
	
	begin
	   inClk <= clk;
		inD <= d;
		inR <= r;
		inP <= p;
	
		unit1: DFlipFlop port map(clk => inClk, d => inD(0), r => inR, p => inP, e => inE, q => outQ(0));
		unit2: DFlipFlop port map(clk => inClk, d => inD(1), r => inR, p => inP, e => inE, q => outQ(1));
		unit3: DFlipFlop port map(clk => inClk, d => inD(2), r => inR, p => inP, e => inE, q => outQ(2));
		unit4: DFlipFlop port map(clk => inClk, d => inD(3), r => inR, p => inP, e => inE, q => outQ(3));
		unit5: DFlipFlop port map(clk => inClk, d => inD(4), r => inR, p => inP, e => inE, q => outQ(4));
		unit6: DFlipFlop port map(clk => inClk, d => inD(5), r => inR, p => inP, e => inE, q => outQ(5));
		unit7: DFlipFlop port map(clk => inClk, d => inD(6), r => inR, p => inP, e => inE, q => outQ(6));
		unit8: DFlipFlop port map(clk => inClk, d => inD(7), r => inR, p => inP, e => inE, q => outQ(7));
		
		q <= outQ;
		
end arch;