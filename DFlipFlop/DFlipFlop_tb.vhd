library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DFlipFlop_tb is
end DFlipFlop_tb;

architecture testbench of DFlipFlop_tb is
	
	component DFlipFlop is
		port(
			clk: in std_logic;
			d	: in std_logic;
			r	: in std_logic;
			p	: in std_logic;
			e	: in std_logic;
			q	: out std_logic
		);
	end component;
	
	signal inClk: std_logic := '0';
	signal inD: std_logic;
	signal inR: std_logic;
	signal inP: std_logic;
	signal inE: std_logic;
	signal inQ: std_logic;
	
	begin
		mapping: DFlipFlop port map(inClk, inD, inR, inP, inE, inQ);
		inClk <= not inClk after 5ns;
		process
			begin
			
				inD <= '0';
				inR <= '0';
				inP <= '0';
				inE <= '0';
				wait for 100ns;
				
				
				inD <= '1';
				inR <= '0';
				inP <= '0';
				inE <= '0';
				wait for 55ns;
				
				
				inD <= '1';
				inR <= '1';
				inP <= '0';
				inE <= '0';
				wait for 50ns;
				
				
				inD <= '0';
				inR <= '0';
				inP <= '1';
				inE <= '1';
				wait for 50ns;
				
				
				inD <= '0';
				inR <= '0';
				inP <= '0';
				inE <= '1';
				wait for 50ns;
				
				
				inE <= '0';
				wait for 50ns;
				
				
				inD <= '1';
				inR <= '1';
				inP <= '1';
				inE <= '0';
				wait for 50ns;
			
		end process;
	
end testbench;