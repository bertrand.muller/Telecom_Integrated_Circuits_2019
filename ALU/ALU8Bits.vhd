 library ieee;
 
 use ieee.std_logic_1164.all;
 use ieee.numeric_std.all;
 
 -- ADD : 001
 -- SUB : 010
 -- AND : 011
 -- OR  : 100
 -- XOR : 101
 
 entity ALU8Bits is
	generic(
		TAILLE: integer := 8
	);
	port(
		a, b	: 	in		std_logic_vector(TAILLE-1 downto 0);
		mode 	:	in 	std_logic_vector(2 downto 0);
		res 	:	out	std_logic_vector(TAILLE-1 downto 0)
	);
end ALU8Bits;


architecture archConc of ALU8Bits is
begin
	process(all)
begin
		case mode is
			when "001"  => res <= std_logic_vector(unsigned(a) + unsigned(b));
			when "010"  => res <= std_logic_vector(unsigned(a) - unsigned(b));
			when "011"  => res <= std_logic_vector(unsigned(a) and unsigned(b));
			when "100"  => res <= std_logic_vector(unsigned(a) or unsigned(b));
			when others => res <= std_logic_vector(unsigned(a) xor unsigned(b));
		end case;
	end process;
end archConc;