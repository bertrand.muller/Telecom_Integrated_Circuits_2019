library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity ALU8Bits_tb is
end ALU8Bits_tb;


architecture testbench of ALU8Bits_tb is

	component ALU8Bits is
		generic(
			TAILLE: integer := 5
		);
	
		port(
			a, b	: 	in		std_logic_vector(TAILLE-1 downto 0);
			mode 	:	in 	std_logic_vector(2 downto 0);
			res 	:	out	std_logic_vector(TAILLE-1 downto 0)
		);
	end component;
	
	signal inA, inB, outRes : std_logic_vector(4 downto 0);
	signal inMode 				: std_logic_vector(2 downto 0); 
	
	begin 
		mapping: ALU8Bits port map(inA, inB, inMode, outRes);
		
		process
			variable errCnt: integer := 0;
			-- Correction
			-- variable cntA,cntB: integer range 0 to 2**5-1
			-- variable cntOp: integer range 0 to 4;
			
		begin
		
			for i in 255 downto 0 loop
				for j in 255 downto 0 loop
					for k in 8 downto 0 loop
						inA <= std_logic_vector(to_signed(i,5));
						inB <= std_logic_vector(to_signed(j,5));
						inMode <= std_logic_vector(to_signed(k,3));
						wait for 15ns;
					end loop;
				end loop;
			end loop;
		
			-- Test ADD
			/* inA <= "00001";
			inB <= "00001";
			inMode <= "001";
			wait for 15ns;
			assert(outRes = "00010") report "Error ADD" severity error;
			if(outRes /= "00010") then
				errCnt := errCnt + 1;
			end if;
			
			
			-- Test SUB
			inA <= "00001";
			inB <= "00001";
			inMode <= "010";
			wait  for 15ns;
			assert(outRes = "00000") report "Error SUB" severity error;
			if(outRes /= "00000") then
				errCnt := errCnt + 1;
			end if;
			
			
			-- Test AND
			inA <= "00001";
			inB <= "00001";
			inMode <= "011";
			wait for 15ns;
			assert(outRes = "00001") report "Error AND" severity error;
			if(outRes /= "00001") then
				errCnt := errCnt + 1;
			end if;
			
			
			-- Test OR
			inA <= "00001";
			inB <= "00001";
			inMode <= "100";
			wait for 15ns;
			assert(outRes = "00001") report "Error OR" severity error;
			if(outRes /= "00001") then
				errCnt := errCnt + 1;
			end if;
			
			
			-- Test XOR
			inA <= "00001";
			inB <= "00001";
			inMode <= "101";
			wait for 15ns;
			assert(outRes = "00000") report "Error XOR" severity error;
			if(outRes /= "00000") then
				errCnt := errCnt + 1;
			end if;
			
			
			if(errCnt = 0) then
				assert false report "OK !" severity note;
			else 
				assert true report "KO !" severity error;
			end if;*/
			
		end process;
	
end testbench;
		