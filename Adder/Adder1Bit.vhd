library ieee;
 
use ieee.std_logic_1164.all;

entity Adder1Bit is
	port(
		a,b,cin  : in  std_logic;
		s,cout   : out std_logic
	);
end Adder1Bit;

architecture archConc of Adder1Bit is
begin
	s    <= a xor b xor cin;
	cout <= (a and b) or (a and cin) or (b and cin);
end archConc;