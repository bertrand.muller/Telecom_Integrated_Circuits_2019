library ieee;
 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Adder4BitsNumericSTD is
	port(
		a, b		 	: in 	std_logic_vector(3 downto 0);
		cin	  		: in  std_logic;
		s 				: out std_logic_vector(3 downto 0);
		cout	    	: out std_logic
	);
end Adder4BitsNumericSTD;


architecture archConc of Adder4BitsNumericSTD is
	signal tmpS : std_logic_vector(4 downto 0);
	signal tmpC : std_logic_vector(3 downto 0);
begin
	
	tmpC(0) <= cin;
	tmpC(3 downto 1) <= (others =>  '0');
	tmpS <= std_logic_vector(unsigned('0' & a) + unsigned('0' & b) + unsigned(tmpC));
	
	s <= tmpS(3 downto 0);
	cout <= tmpS(4);
	
end archConc;