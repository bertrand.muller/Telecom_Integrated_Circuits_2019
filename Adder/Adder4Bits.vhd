library ieee;
 
use ieee.std_logic_1164.all;

entity Adder4Bits is
	port(
		inA, inB 	: in std_logic_vector(3 downto 0);
		inCin  		: in  std_logic;
		outS 			: out std_logic_vector(3 downto 0);
		outCout    	: out std_logic
	);
end Adder4Bits;

architecture archConc of Adder4Bits is

	component Adder1Bit is
		port(
			a, b, cin : in std_logic;
			s, cout : out std_logic
		);
	end component;
	
	signal sCout : std_logic_vector(2 downto 0);
	
begin

	-- First Adder 1 Bit
	unit1: Adder1Bit
		port map (a => inA(0), b => inB(0), cin => inCin, s => outS(0), cout => sCout(0));
	
	-- Second Adder 1 Bit
	unit2: Adder1Bit
		port map (a => inA(1), b => inB(1), cin => sCout(0), s => outS(1), cout => sCout(1));
		
	-- Third Adder 1 Bit
	unit3: Adder1Bit
		port map(a => inA(2), b => inB(2), cin => sCout(1), s => outS(2), cout => sCout(2));
		
	-- Fourth Adder 1 Bit
	unit4: Adder1Bit
		port map(a => inA(3), b => inB(3), cin => sCout(2), s => outS(3), cout => outCout);
	
end archConc;