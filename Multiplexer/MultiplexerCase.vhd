library ieee;

use ieee.std_logic_1164.all;

entity MultiplexerCase is
	port(
		IN0,IN1,IN2,IN3: in  std_logic_vector(7 downto 0);
		Selecteur		: in  std_logic_vector(1 downto 0);
		Sortie 			: out std_logic_vector(7 downto 0)
	);
end MultiplexerCase;

architecture ArchCase of MultiplexerCase is begin
	process(Selecteur,IN0,IN1,IN2,IN3)
	--process(all) –VHDL 2008
	begin
		case Selecteur is
			when "00" => Sortie <= IN0;
			when "01" => Sortie <= IN1;
			when "10" => Sortie <= IN2;
			when "11" => Sortie <= IN3;
			when others => Sortie <= (others => '0');
		end case;
	end process;
end ArchCase;