library ieee;
 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 

entity ALU is
	generic(
		TAILLE: integer := 4
	);
	port(
		a, b	: 	in		std_logic_vector(TAILLE-1 downto 0);
		c 		:	in 	std_logic_vector(1 downto 0);
		s 		:	out	std_logic_vector(TAILLE-1 downto 0)
	);
end ALU;


architecture arch of ALU is
begin
	process(all)
begin
		case c is
			when "00"  => s <= std_logic_vector(unsigned(a) and unsigned(b));
			when "01"  => s <= std_logic_vector(unsigned(a) or unsigned(b));
			when "10"  => s <= std_logic_vector(unsigned(a) xor unsigned(b));
			when others => s <= std_logic_vector(unsigned(a) + unsigned(b));
		end case;
	end process;
end arch;