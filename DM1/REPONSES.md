# Télécom Nancy SCIN 2019 - DM1 -  _Réponses_

### Première Version

1. Voir le fichier ```FirstVersion.vhd```

2. Nombre de comparateurs : **<u>11</u>** (Voir le fichier ```First Version - RTL View.pdf``` pour vérification)<br/>
   Nombre de multiplexeurs 2 -> 1 pour **32** bits signés : **<u>0</u>**<br/>
   Nombre de multiplexeurs 3 -> 1 pour **32** bits signés : **<u>261</u>**
3. Fréquence maximale : ```Fmax = 176.03MHz ```
4. Voir le fichier ```FirstVersion_tb.vhd```



### Deuxième Version

1. Voir le fichier ```SecondVersion.vhd```
2. Le nombre de comparateurs est bien de **<u>3</u>** (Voir le fichier ```Second Version - RTL View.pdf``` pour vérification).<br/>
   Nombre de multiplexeurs 2 -> 1 pour **32** bits signés : **<u>0</u>**<br/>
   Nombre de multiplexeurs 3 -> 1 pour **32** bits signés : **<u>69</u>**

3. Fréquence maximale : ```Fmax = 193.58MHz```

4. Voir le fichier ```SecondVersion_tb.vhd```

   

### Troisième Version

1. Voir le fichier ```ThirdVersion.vhd```

2. Le nombre de comparateurs est bien de **<u>3</u>** (Voir le fichier ```Third Version - RTL View.pdf``` pour vérification).<br/>
   Nombre de multiplexeurs 2 -> 1 sur **32** bits signés : **<u>0</u>**<br/>
   Nombre de multiplexeurs 3 -> 1 sur **32** bits signés : **<u>321</u>**

3. Fréquence maximale : ```Fmax = 296.33MHz```

4. Voir le fichier ```ThirdVersion_tb.vhd```

   