library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SecondVersion is
	generic(
		Nbits : integer := 32
	);
	port(
		clk				: in 	std_logic;
		A, B, C			: in 	std_logic_vector(Nbits-1 downto 0);
		Min, Med, Max	: out std_logic_vector(Nbits-1 downto 0)
	);
end SecondVersion;

architecture arch of SecondVersion is

signal regSigA, regSigB, regSigC			: std_logic_vector(Nbits-1 downto 0);
signal regSigMin, regSigMed, regSigMax : std_logic_vector(Nbits-1 downto 0);
signal minAB, maxAB 							: std_logic_vector(Nbits-1 downto 0);

component RegisterS
	generic(
		N : integer := Nbits
	);
	port(
		clk: in std_logic;
		d: in std_logic_vector(N-1 downto 0);
		q: out std_logic_vector(N-1 downto 0)
	);
end component; 

begin

	regA : RegisterS port map(clk, A, regSigA);
	regB : RegisterS port map(clk, B, regSigB);
	regC : RegisterS port map(clk, C, regSigC);
	
	regMin : RegisterS port map(clk, regSigMin, Min);
	regMed : RegisterS port map(clk, regSigMed, Med);
	regMax : RegisterS port map(clk, regSigMax, Max);
	
	process(regSigA, regSigB, regSigC, minAB, maxAB)
		begin
			
				-- Calculate Min & Max between A & B
				if regSigA > regSigB then
					maxAB <= regSigA;
					minAB <= regSigB;
				else
					maxAB <= regSigB;
					minAB <= regSigA;
				end if;
				
				-- Compare minAB & maxAB with C
				if regSigC <= minAB then
					regSigMin <= regSigC;
					regSigMed <= minAB;
					regSigMax <= maxAB;
				elsif regSigC >= maxAB then
					regSigMax <= regSigC;
					regSigMed <= maxAB;
					regSigMin <= minAB;
				else
					regSigMin <= MinAB;
					regSigMed <= regSigC;
					regSigMax <= MaxAB;
				end if;
	end process;
end arch;