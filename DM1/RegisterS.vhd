library ieee;
 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 

entity RegisterS is
	generic(
		N: integer := 32
	);
	port(
		clk	: in std_logic;
		d		: in std_logic_vector(N-1 downto 0);
		q 		: out std_logic_vector(N-1 downto 0)
	);
end RegisterS;


architecture archRegister of RegisterS is
begin
	process(clk)
		begin
			if(clk'event and clk = '1') then
				q <= d;
			end if;
	end process;
end archRegister;