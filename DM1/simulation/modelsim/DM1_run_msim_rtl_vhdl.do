transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -2008 -work work {D:/Projets/Telecom_Integrated_Circuits_2019/DM1/FirstVersion.vhd}
vcom -2008 -work work {D:/Projets/Telecom_Integrated_Circuits_2019/DM1/RegisterS.vhd}

vcom -2008 -work work {D:/Projets/Telecom_Integrated_Circuits_2019/DM1/FirstVersion_tb.vhd}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cyclonev -L rtl_work -L work -voptargs="+acc"  FirstVersion_tb

add wave *
view structure
view signals
run -all
