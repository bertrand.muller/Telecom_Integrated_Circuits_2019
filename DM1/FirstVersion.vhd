library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FirstVersion is
	generic(
		Nbits : integer := 32
	);
	port(
		clk				: in  std_logic;
		A, B, C			: in  std_logic_vector(Nbits-1 downto 0);
		Min, Med, Max	: out std_logic_vector(Nbits-1 downto 0)
	);
end FirstVersion;

architecture arch of FirstVersion is

signal regSigA, regSigB, regSigC			: std_logic_vector(Nbits-1 downto 0);
signal regSigMin, regSigMed, regSigMax : std_logic_vector(Nbits-1 downto 0);

component RegisterS
	generic (
		N : integer := Nbits
	);
	port(
		clk: in std_logic;
		d: in std_logic_vector(N-1 downto 0);
		q: out std_logic_vector(N-1 downto 0)
	);
end component; 

begin

	regA : RegisterS port map(clk, A, regSigA);
	regB : RegisterS port map(clk, B, regSigB);
	regC : RegisterS port map(clk, C, regSigC);
	
	regMin : RegisterS port map(clk, regSigMin, Min);
	regMed : RegisterS port map(clk, regSigMed, Med);
	regMax : RegisterS port map(clk, regSigMax, Max);
 		
	process(regSigA, regSigB, regSigC)
		begin	
			/* MAX */
			-- 1
			if regSigA > regSigB then
				-- 2
				if regSigA > regSigC then
					regSigMax <= regSigA;
				else
					regSigMax <= regSigC;
				end if;
			else
				-- 3
				if regSigB > regSigC then
					regSigMax <= regSigB;
				else
					regSigMax <= regSigC;
				end if;
			end if;
					
			/* MIN */
			-- 1
			if regSigA < regSigB then
				-- 2
				if regSigA < regSigC then
					regSigMin <= regSigA;
				else	
					regSigMin <= regSigC;
				end if;
			else	
				-- 2
				if regSigB < regSigC then
					regSigMin <= regSigB;
				else
					regSigMin <= regSigC;
				end if;
			end if;
				
			/* MED */
			-- 1
			if regSigA > regSigB then
				-- 2
				if regSigA < regSigC then
					regSigMed <= regsigA;
				else
					-- 3 
					if regSigB > regSigC then
						regSigMed <= regSigB;
					else
						regSigMed <= regSigC;
					end if;
				end if;
			else
				-- 4
				if regSigA > regSigC then
					regSigMed <= regSigA;
				else
					-- 5
					if regSigB > regSigC then
						regSigMed <= regSigC;
					else
						regSigMed <= regSigB;
					end if;
				end if;
			end if;
	end process;
end arch;