library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ThirdVersion is
	generic(
		Nbits : integer := 32
	);
	port(
		clk				: in 	std_logic;
		A, B, C			: in 	std_logic_vector(Nbits-1 downto 0);
		Min, Med, Max	: out std_logic_vector(Nbits-1 downto 0)
	);
end ThirdVersion;

architecture arch of ThirdVersion is

signal regSigA, regSigB, regSigC			: std_logic_vector(Nbits-1 downto 0);
signal regSigMin, regSigMed, regSigMax : std_logic_vector(Nbits-1 downto 0);
signal aLTb, aLTc, bLTc 					: boolean;

component RegisterS
	generic(
		N : integer := Nbits
	);
	port(
		clk	: in 	std_logic;
		d		: in 	std_logic_vector(Nbits-1 downto 0);
		q		: out std_logic_vector(Nbits-1 downto 0)
	);
end component;

begin

	regA : RegisterS port map(clk, A, regSigA);
	regB : RegisterS port map(clk, B, regSigB);
	regC : RegisterS port map(clk, C, regSigC);
	
	regMin : RegisterS port map(clk, regSigMin, Min);
	regMed : RegisterS port map(clk, regSigMed, Med);
	regMax : RegisterS port map(clk, regSigMax, Max);

	process(regSigA, regSigB, regSigC, aLTb, aLTc, bLTc)
		begin
				
				aLTb <= (regSigA < regSigB);
				aLTc <= (regSigA < regSigC);
				bLTc <= (regSigB < regSigC);
				
				-- A
				if (aLTb and aLTC) then
					regSigMin <= regSigA;
				elsif (not aLTb and not aLTc) then
					regSigMax <= regSigA;
				else
					regSigMed <= regSigA;
				end if;
				
				
				-- B
				if (bLTc and not aLTb) then
					regSigMin <= regSigB;
				elsif (not bLTc and aLTb) then
					regSigMax <= regSigB;
				else
					regSigMed <= regSigB;
				end if;
				
				
				-- C
				if (not bLTc and not aLTC) then
					regSigMin <= regSigC;
				elsif (bLTC and aLTC) then
					regSigMax <= regSigC;
				else
					regSigMed <= regSigC;
				end if;
	end process;
end arch;