library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SecondVersion_tb is
end SecondVersion_tb;


architecture testbench of SecondVersion_tb is
	component SecondVersion is
		generic(
			Nbits : integer := 5
		);
		port(
			clk				: in std_logic;
			A, B, C			: in std_logic_vector(Nbits-1 downto 0);
			Min, Med, Max	: out std_logic_vector(Nbits-1 downto 0)
		);
	end component;
	
	constant Nbits						: integer := 5; 
	signal inClk						: std_logic := '0';
	signal inA, inB, inC				: std_logic_vector(Nbits-1 downto 0);
	signal outMin, outMed, outMax	: std_logic_vector(Nbits-1 downto 0);
	
	begin
		mapping: SecondVersion port map(inClk, inA, inB, inC, outMin, outMed, outMax);
		inClk <= not inClk after 5ns;
		
		process
			begin
			
				-- A < B < C
				inA <= "00001";
				inB <= "00010";
				inC <= "00011";
				wait for 15ns;
				
				-- B < A < C
				inB <= "00100";
				inA <= "00101";
				inC <= "00110";
				wait for 15ns;
				
				-- B < C < A
				inB <= "00111";
				inC <= "01000";
				inA <= "01001";
				wait for 15ns;
				
				-- A < C < B
				inA <= "01010";
				inC <= "01011";
				inB <= "01100";
				wait for 15ns;
				
				-- C < A < B
				inC <= "01101";
				inA <= "01110";
				inB <= "01111";
				wait for 15ns;
				
				-- C < B < A
				inC <= "10000";
				inB <= "10001";
				inA <= "10010";
				wait for 15ns;
				
		end process;
end testbench;