transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {T:/SCIN/Telecom_Integrated_Circuits_2019/StatesMachine/raisingedge_teddy.vhd}

vcom -93 -work work {T:/SCIN/Telecom_Integrated_Circuits_2019/StatesMachine/RaisingEdge_tb_Teddy.vhd}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cyclonev -L rtl_work -L work -voptargs="+acc"  RaisingEdge_tb_Teddy

add wave *
view structure
view signals
run -all
