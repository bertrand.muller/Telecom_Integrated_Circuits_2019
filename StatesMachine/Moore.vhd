library ieee;

use ieee.std_logic_1164.all;

entity Moore is
port(
	clk, reset, strobe: in std_logic;
	p1: out std_logic
);
end Moore ;

architecture Moore_arch of Moore is
	type mc_state_type is (zero, edge, one);
	signal state_reg, state_next: mc_state_type;
	attribute enum_encoding: string;
	attribute enum_encoding of mc_state_type: type is "0000 0100 1000";

	begin
		-- State register
		process(clk, reset)
		begin
			if(reset='1') then
				state_reg <= zero;
			elsif(clk'event and clk='1') then
				state_reg <= state_next;
			end if;
		end process;
		
		-- Next-state logic
		process(state_reg, strobe)
		begin
			case state_reg is
				when zero =>
					if (strobe='1') then
						state_next <= edge;
					end if;
					
				when edge =>
					if(strobe='1') then
						state_next <= one;
					else
						state_next <= zero;
					end if;
					
				when one =>
					if(strobe='0') then
						state_next <= zero;
					end if;
			end case;
		end process;
		
		-- Moore output logic
		process(state_reg)
		begin
			p1 <= '0';
			case state_reg is
				when edge =>
					p1 <= '1';
				when one =>
				when zero =>
					p1 <= '0';
			end case;
		end process;
end Moore_arch;