library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RaisingEdge_Teddy is
	port(
	strobe, reset, clk  : in  std_logic;
	rs  : out std_logic);
end RaisingEdge_Teddy;

architecture archREMoore of RaisingEdge_Teddy is
	type state_type is (zero, edge, one);
	signal state_reg, state_next: state_type;
begin
	-- Registre d'état
	process(clk, reset)
	begin
		if(reset='1') then
			state_reg <= zero;
		elsif (clk'event and clk='1') then
			state_reg <= state_next;
		end if;
	end process;
	
	-- Logique de l'état futur
	process(state_reg, strobe)
	begin
		case state_reg is
			when zero =>
				if strobe='1' then
					state_next <= edge;
				else
					state_next <= zero;
				end if;
			when edge =>
				if strobe='0' then
					state_next <= zero;
				else
					state_next <= one;
				end if;
			when one =>
				if strobe='0' then
					state_next <= zero;
				else
					state_next <= one;
				end if;
		end case;
	end process;
	
	-- Génération de sortie
	process(state_reg)
	begin
		rs <= '0';
		case state_reg is
			when edge => rs <= '1';
			when zero =>
			when one =>
		end case;
	end process;
	
end archREMoore;

architecture archREMealy of RaisingEdge_Teddy is
	type state_type is (zero, delay, one);
	signal state_reg, state_next: state_type;
begin
	-- Registre d'état
	process(clk, reset)
	begin
		if(reset='1') then
			state_reg <= zero;
		elsif (clk'event and clk='1') then
			state_reg <= state_next;
		end if;
	end process;
	
	-- Logique de l'état futur
	process(state_reg, strobe)
	begin
		case state_reg is
			when zero =>
				if strobe='1' then
					state_next <= delay;
				else
					state_next <= zero;
				end if;
			when delay =>
				if strobe='0' then
					state_next <= zero;
				else
					state_next <= one;
				end if;
			when one =>
				if strobe='0' then
					state_next <= zero;
				else
					state_next <= one;
				end if;
		end case;
	end process;
	
	-- Génération de sortie
	process(state_reg, strobe)
	begin
		rs <= '0';	
		case state_reg is
			when zero =>
				if strobe='1' then
					rs <= '1';
				end if;
			when delay => rs <= '1';
			when one =>
		end case;
	end process;
	
end archREMealy;