 library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Mealy_tb is
end Mealy_tb;

architecture testbench of Mealy_tb is
	component Mealy is
		port(
			clk, reset, strobe: in std_logic;
			p3: out std_logic
		);
	end component;
	
	signal clkA, resetA, strobeA, p3A : std_logic := '0';
	
	begin
		mapping: Mealy port map(clkA, resetA, strobeA, p3A);
		
		clkA <= not clkA after 5ns;
		strobeA <= not strobeA after 20ns;
		
end testbench;