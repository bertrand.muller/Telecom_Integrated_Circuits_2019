library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RaisingEdge_tb_Teddy is
end RaisingEdge_tb_Teddy;

architecture tb of RaisingEdge_tb_Teddy is
	component RaisingEdge_Teddy is
		port(
		strobe, reset, clk  : in  std_logic;
		rs  : out std_logic);
	end component;
	signal  inStrobe, inReset, inClk, outRs, outRsMe : std_logic := '0';
	
	begin
		mappingMoore: entity work.RaisingEdge_Teddy(archREMoore) port map(inStrobe, inReset, inClk, outRs);
		mappingMealy: entity work.RaisingEdge_Teddy(archREMealy) port map(inStrobe, inReset, inClk, outRsMe);
		
		inClk <= not inClk after 5ns;
		inStrobe <= not inStrobe after 147ns;
end tb;