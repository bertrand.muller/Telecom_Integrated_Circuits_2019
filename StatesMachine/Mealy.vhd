library ieee;

use ieee.std_logic_1164.all;

entity Mealy is
port(
	clk, reset, strobe: in std_logic;
	p3: out std_logic
);
end Mealy ;

architecture Mealy_arch of Mealy is
	type mc_state_type is (zero, delay, one);
	signal state_reg, state_next: mc_state_type;
	attribute enum_encoding: string;
	attribute enum_encoding of mc_state_type: type is "0000 0100 1000";

	begin
		-- State register
		process(clk, reset)
		begin
			if(reset='1') then
				state_reg <= zero;
			elsif(clk'event and clk='1') then
				state_reg <= state_next;
			end if;
		end process;
		
		-- Next-state logic
		process(state_reg, strobe)
		begin
			case state_reg is
				when zero =>
					if (strobe='1') then
						state_next <= delay;
					end if;
					
				when delay =>
					if(strobe='1') then
						state_next <= one;
					else
						state_next <= zero;
					end if;
					
				when one =>
					if(strobe='0') then
						state_next <= zero;
					end if;
					
			end case;
		end process;
		
		-- Moore output logic
		process(state_reg, strobe)
		begin
			p3 <= '0';
			case state_reg is
				when one =>
					p3 <= '1';
				when zero =>
					if strobe='1' then
						p3 <= '1';
					else
						p3 <= '0';
					end if;
			end case;
		end process;
end Mealy_arch;