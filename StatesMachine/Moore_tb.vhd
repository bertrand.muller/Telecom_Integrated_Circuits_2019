library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Moore_tb is
end Moore_tb;

architecture testbench of Moore_tb is
	component Moore is
		port(
			clk, reset, strobe: in std_logic;
			p1: out std_logic
		);
	end component;
	
	signal clkA, resetA, strobeA, p1A : std_logic := '0';
	
	begin
		mapping: Moore port map(clkA, resetA, strobeA, p1A);
		
		clkA <= not clkA after 5ns;
		strobeA <= not strobeA after 20ns;
		
end testbench;