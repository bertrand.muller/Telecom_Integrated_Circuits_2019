library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CounterModulo10FPGA is
	port(
		CLOCK_50	: in  std_logic;
		SW       	: in  std_logic_vector(9 downto 0);
		KEY		: in  std_logic_vector(3 downto 0);
		HEX0		: out std_logic_vector(6 downto 0);
		HEX1		: out std_logic_vector(6 downto 0)
	);
end CounterModulo10FPGA;

architecture arch of CounterModulo10FPGA is
	component CounterModulo10 is
		port(
			clk	: in  std_logic;
			v		: in  std_logic;
			TC		: out std_logic;
			q		: out std_logic_vector(3 downto 0)
		);
	end component;
	
	signal qOutU, qOutD 		: std_logic_vector(3 downto 0);
	signal tcOutU, tcOutD 	: std_logic;
	signal hx0, hx1 			: std_logic_vector(6 downto 0);
	
	begin
		unit: CounterModulo10
			port map(clk => CLOCK_50, v => KEY(0), TC => tcOutU, q => qOutU);
		
		unit2: CounterModulo10
			port map(clk => CLOCK_50, v => tcOutU, TC => tcOutD, q => qOutD);
		--HEX0 <= "0000000";
		--process(qOutU)
		process(SW)
			begin
				--case qOutU is
			   case SW(3 downto 0) is	
				when "0000" => hx0 <= "0111111"; -- 0
					when "0001" => hx0 <= "0000110"; -- 1
					when "0010" => hx0 <= "1011011"; -- 2
					when "0011" => hx0 <= "1001111"; -- 3
					when "0100" => hx0 <= "1100110"; -- 4
					when "0101" => hx0 <= "1101101"; -- 5
					when "0110" => hx0 <= "1111101"; -- 6
					when "0111" => hx0 <= "0000111"; -- 7
					when "1000" => hx0 <= "1111111"; -- 8
					when "1001" => hx0 <= "1101111"; -- 9
					when others => hx0 <= "1000000";
				end case;
				
				--HEX0 <= "1010101"; --not hx0;
		end process;
		HEX0 <= not hx0;
		--		

--		process(qOutD)
--			begin
--				case qOutD is
--					when "0000" => hx1 <= "0111111"; -- 0
--					when "0001" => hx1 <= "0000110"; -- 1
--					when "0010" => hx1 <= "1011011"; -- 2
--					when "0011" => hx1 <= "1001111"; -- 3
--					when "0100" => hx1 <= "1100110"; -- 4
--					when "0101" => hx1 <= "1101101"; -- 5
--					when "0110" => hx1 <= "1111101"; -- 6
--					when "0111" => hx1 <= "0000111"; -- 7
--					when "1000" => hx1 <= "1111111"; -- 8
--					when "1001" => hx1 <= "1101111"; -- 9
--					when others => hx1 <= "1000000";
--				end case;
--				
--				HEX1 <= not hx1;
--		end process;
--		
end architecture;