library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CounterModulo10 is
	port(
		clk 			: in  	std_logic;
		v				: in  	std_logic;
		TC		 		: out 	std_logic;
		q		  		: out 	std_logic_vector(3 downto 0)
	);
end;

architecture behavior of CounterModulo10 is
	begin
		process(clk)
		variable count:unsigned(3 downto 0) := (others => '0');
		begin
			if (clk'event and clk = '1') then
				if (v = '1') then
					count := count + 1;
					if(unsigned(count) > 9) then
						count := (others => '0');
						TC <= '1';
					else
						TC <= '0';
					end if;
				end if;
			end if;
			q <= std_logic_vector(count);
		end process;
end behavior;