library ieee;
 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity Comparater is
	port(
		a, b 					: in  std_logic_vector(7 downto 0); -- 8 bits entries
		agtb, altb, aeqb 	: out std_logic
	);
end Comparater;


architecture archConc of Comparater is
begin
	agtb <= '1' when unsigned(a) > unsigned(b) else '0';
	altb <= '1' when unsigned(a) < unsigned(b) else '0';
	aeqb <= '1' when unsigned(a) = unsigned(b) else '0';
end archConc;