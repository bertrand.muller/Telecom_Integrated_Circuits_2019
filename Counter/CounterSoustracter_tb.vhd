library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CounterSoustracter_tb is
end CounterSoustracter_tb;

architecture testbench of CounterSoustracter_tb is
	
	component CounterSoustracter	is
		generic(
			size: integer := 8
		);
	
		port(
			clk: in std_logic;
			d: in std_logic_vector(size-1 downto 0);
			r: in std_logic;
			l: in std_logic;
			e: in std_logic;
			s: in std_logic;
			q: out std_logic_vector(size-1 downto 0)
		);
	end component;
	
	constant size: integer := 8;
	signal inClk: std_logic := '0';
	signal inR, inL, inE, inS: std_logic;
	signal inD, outQ: std_logic_vector(size-1 downto 0);
	
	begin
		mapping: CounterSoustracter port map(inClk, inD, inR, inL, inE, inS, outQ);
		inClk <= not inClk after 5ns;
		process
			begin
			
				-- Croissant
				inD <= (others => '0');
				inR <= '1';
				inL <= '0';
				inE <= '1';
				inS <= '1';
				wait for 100ns;
			
				inR <= '0';
				inL <= '0';
				inE <= '1';
				inS <= '1';
				wait for 1000ns;
				
				inD <= x"04";
				inR <= '0';
				inL <= '1';
				inE <= '0';
				inS <= '1';
				wait for 1000ns;
				
				inR <= '0';
				inL <= '0';
				inE <= '1';
				inS <= '1';
				wait for 1000ns;
			
				
				-- Décroissant
				inD <= (others => '0');
				inR <= '1';
				inL <= '0';
				inE <= '1';
				inS <= '0';
				wait for 100ns;
			
				inR <= '0';
				inL <= '0';
				inE <= '1';
				inS <= '0';
				wait for 1000ns;
				
				inD <= x"04";
				inR <= '0';
				inL <= '1';
				inE <= '0';
				inS <= '0';
				wait for 1000ns;
				
				inR <= '0';
				inL <= '0';
				inE <= '1';
				inS <= '0';
				wait for 1000ns;
			
		end process;
	
end testbench;