library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CounterSoustracter is
	generic(
		size: integer := 8
	);
	
	port(
		clk: in std_logic;
		d: in std_logic_vector(size-1 downto 0);
		r: in std_logic;
		l: in std_logic;
		e: in std_logic;
		s: in std_logic;
		q: out std_logic_vector(size-1 downto 0)
	);
end CounterSoustracter;

architecture arch of CounterSoustracter is
begin
	process(clk, r, l)
		begin
			if (r = '1') then
				q <= (others => '0');
			elsif (l = '1') then
				q <= d;
			elsif (clk'event and clk = '1') then
				if (e = '1') then
				-- if rising_edge(clk) then
					if(s = '1') then
						q <= std_logic_vector(unsigned(q)+1);
					else 
						q <= std_logic_vector(unsigned(q)-1);
					end if;
				end if;
			end if;
	end process;
end arch;