library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Counter_tb is
end Counter_tb;

architecture testbench of Counter_tb is
	
	component Counter is
		generic(
			size: integer := 8
		);
	
		port(
			clk: in std_logic;
			d: in std_logic_vector(size-1 downto 0);
			r: in std_logic;
			l: in std_logic;
			e: in std_logic;
			q: out std_logic_vector(size-1 downto 0)
		);
	end component;
	
	constant size: integer := 8;
	signal inClk: std_logic := '0';
	signal inR, inL, inE: std_logic;
	signal inD, outQ: std_logic_vector(size-1 downto 0);
	
	begin
		mapping: Counter port map(inClk, inD, inR, inL, inE, outQ);
		inClk <= not inClk after 5ns;
		process
			begin
			
				inD <= (others => '0');
				inR <= '1';
				inL <= '0';
				inE <= '1';
				wait for 100ns;
			
				inR <= '0';
				inL <= '0';
				inE <= '1';
				wait for 1000ns;
				
				inD <= x"04";
				inR <= '0';
				inL <= '1';
				inE <= '0';
				wait for 1000ns;
				
				inR <= '0';
				inL <= '0';
				inE <= '1';
				wait for 1000ns;
			
		end process;
	
end testbench;